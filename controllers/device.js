const Device = require('../models/device')
const DeviceTraffic = require('../models/deviceTraffic')
const moment = require('moment')
const User = require('../models/users')
var logger = global.logger;
var config = global.config;
var mongoose = require('mongoose');

function get_seller_name(selleList, seller_code)
{
  if(selleList!=null)
  {
    for(let index=0; index<selleList.length; index++)
    {
        if(selleList[index].seller_code==seller_code)
          return selleList[index].name;
    }
  }
  return "#";
}

function makeReturnDevice(index, orgdevice, trafficInfo, user_level, selleList)
{
  const emtty_agency_data = {
    username : '',
    usertel : '',
    address : '', 
    order_date : '', 
    remark : ''
  }

  const mod_agency_data = orgdevice.agency_data.username==null ? emtty_agency_data : orgdevice.agency_data;
  const newdevice = {
    index:index, 
    _id : orgdevice._id,
    mac : orgdevice.mac,
    model : orgdevice.model,
    order_num : orgdevice.order_num,
    ssr_ip : orgdevice.ssr_ip,
    ssr_ip_cn : orgdevice.ssr_ip_cn,
    vpn_off : orgdevice.vpn_off,
    twoway_service : orgdevice.twoway_service,
    fullmode : orgdevice.fullmode,
    dns_type : orgdevice.dns_type,
    dns_proxy : orgdevice.dns_proxy,
    dnsmasq : orgdevice.dnsmasq,
    ssr_speed : orgdevice.ssr_speed,
    last_activated_status : orgdevice.last_activated_status,
    ssr_expired_date : orgdevice.ssr_expired_date,
    last_activated_date :  moment(orgdevice.last_activated_date).format('YYYY-MM-DD HH:mm:ss'),
    last_activation_count : orgdevice.last_activation_count,
    ip_update_count : orgdevice.ip_update_count,
    forced_activation_count : orgdevice.forced_activation_count,
    activation_count : orgdevice.activation_count,
    group : orgdevice.group,
    kcptun : orgdevice.kcptun,
    first_activation : orgdevice.first_activation,
    last_activated_ip : orgdevice.last_activated_ip,
    activation_date : orgdevice.activation_date,
    geo_isp : orgdevice.geo_isp,
    geo_city : orgdevice.geo_city,
    geo_country : orgdevice.geo_country,
    version : orgdevice.version,
    pkg_version : orgdevice.pkg_version,
    public_ip : orgdevice.public_ip,
    agency_data : mod_agency_data, //orgdevice.agency_data || emtty_agency_data,
    uptime : orgdevice.uptime,
    traffics : trafficInfo
  }

  if(user_level=="0")
  {
    newdevice['remark'] = orgdevice.remark;
    newdevice['agency_code'] = orgdevice.agency_code;
    newdevice['last_ssid'] = orgdevice.last_ssid;
  }

  //if(user_level=="0" || user_level=="1")
  {
    newdevice['seller_code'] = orgdevice.seller_code;

    if(orgdevice.seller_code!="" && orgdevice.seller_code!=null)
      newdevice['seller_name'] = get_seller_name(selleList, orgdevice.seller_code);
    else
      newdevice['seller_name'] = "-";
  }
  return newdevice;
}

async function findAll (ctx) {
  let sort_order = {last_activated_date:-1};
  let pageNumber = parseInt(ctx.request.query.pageNmber || 1, 10);
  let paginate = parseInt(ctx.request.query.paginate || 10, 10);
  let key_type = ctx.request.query.type || "";
  let key_val  = ctx.request.query.val || "";
  let agency_code = ctx.request.query.agency_code || "";
  let user_key = ctx.request.query.user_key || ""; 
  let user_level = "2";
  let user_id = "";

  if (pageNumber<1 || agency_code=="")
  {
    logger.info("%s [%s] Find Device. Invalid request!", ctx.method, ctx.ip)
    ctx.status = 400;
    return;
  }

  if(ctx.request.query.sort_val!=null)
  {
    sort_aced = parseInt(ctx.request.query.sort_aced || -1, 10);
    sort_order = {
      [ctx.request.query.sort_val]:sort_aced
    }
  }

  try {
    // check agency code
    user_query = { $or: [{agency_code:agency_code}, {seller_code:agency_code}] };
    if(user_key!=null && user_key!=="")
      user_query["_id"] = mongoose.Types.ObjectId(user_key);
    const userData = await User.findOne(user_query)
    //console.log("[%s]", userData)
    if(!userData)
    {
      ctx.status = 401;
      logger.info("%s [%s] Find Device. Invalid User, User Key [%s] Agency Code [%s]!", ctx.method, ctx.ip, user_key, agency_code)
      ctx.body = {
        message: "Not Auth"
      }
      return;
    }
    user_level = userData.level;
    user_id= userData.id;
  } catch (error) {
     ctx.throw(500, error);
     return;
  }

  let query = {};
  let seller_query = {};
  if(user_level=="1")
  {
    query["agency_code"] = agency_code;
    seller_query["agency_code"] = agency_code;
    seller_query["level"] = "2";
  }
  else if(user_level=="2")  // level 2의 경우 agency_code가 seller_code이다. 
  {
    query["seller_code"] = agency_code;
  }

  if(key_type=="mac")
  {
    key_val = key_val.replace(/:/g, '');
  }

  const regex_str=key_val
  const regexval = new RegExp(regex_str.toLowerCase().trim(), 'i')
  const online_time = process.env.ONLINE_SESSION_TIME || "2";
  var activatedTime = moment().add(parseInt(online_time)*-1, 'hours');
  const regexval_error = new RegExp("v0")

  switch(key_type) {
    case 'mac' :
      query["mac"] = {$regex:regexval};
      break;
    case 'username' :
      query["agency_data.username"] = {$regex: regexval};
      break;
    case  'usertel' :
      query["agency_data.usertel"] = {$regex: regexval};
      break;
    case  'seller' :
      seller_query["name"] = {$regex: regexval};
      break;
    case  'recent' :
      query['last_activated_date'] = {$gte: activatedTime};
      break;
    case  'recentError' :
      query['last_activated_date'] = {$gte: activatedTime};
      query['last_activated_status'] = {$regex: regexval_error};
      break;
    default : 
  }

  //console.log("query:", query);
  try {
    let device_query = {};

    if(user_level=="1")
      device_query["agency_code"] = agency_code;
    else if(user_level=="2")  // level 2의 경우 agency_code가 seller_code이다. 
      device_query["seller_code"] = agency_code;

    const total_device = await Device.countDocuments(device_query);

    device_query['last_activated_date'] = {$gte: activatedTime};
    const active_device = await Device.countDocuments(device_query);

    device_query['last_activated_status'] = {$regex: regexval_error};
    const active_error_device = await Device.countDocuments(device_query);
    const selleList = await User.find({level:"2"});

    if(key_type=="seller")
    {
      //console.log("seller_query:", seller_query);
      const sellerData = await User.find(seller_query, {_id:0, name:1, seller_code:1});

      if(sellerData.length==0)
      {
        query["mac"] = "NONE";   // to make empty result
      }
      else
      {
        let sellerCode = [];
        for (let index = 0; index < sellerData.length; index++) {
          sellerCode.push({"seller_code":sellerData[index].seller_code});
        }
        query['$or'] = sellerCode;
      }
    }
    //console.log("query:", query);

    var devices = await Device.find(query).sort(sort_order).skip((pageNumber-1)*paginate).limit(paginate)
    const deviceCount = await Device.countDocuments(query);
    var result_array= [];

    // update traffic information
    for (let index = 0; index < devices.length; index++) {
      let device_mac = devices[index].mac;
      let traffic_query = {mac : device_mac}
      let max_traffic = 15;

      const trafficInfo = await DeviceTraffic.find(
        traffic_query,
        {traffic_date:1, mac:1, download:1, upload:1, ssr_speed:1, last_activated_status:1, ip_update_count:1, activation_count:1} 
      ).sort({traffic_date:-1}).limit(max_traffic);

      const newdevice = makeReturnDevice(index, devices[index], trafficInfo, user_level, selleList);

      //console.log(newdevice);
      result_array.push(newdevice)
      //console.log(result_array)
    }
    logger.info("%s [%s] Find Device. User ID:%s, Agency Code:%s, Search Type:%s, Search Result:%d, Total Device:%d, Active Device:%d, Error Device:%d !", 
            ctx.method, ctx.ip, user_id, agency_code, key_type,
            deviceCount, total_device, active_device,active_error_device);

    ctx.set("Access-Control-Expose-Headers", "Last-Page, Total-Device, Active-Device, Active-Error-Device");
    ctx.set("Last-Page", Math.ceil(deviceCount/paginate).toString());
    ctx.set("Total-Device", total_device.toString() || "0");
    ctx.set("Active-Device", active_device.toString() || "0");
    ctx.set("Active-Error-Device", active_error_device.toString() || "0");
    ctx.body = result_array;
    
  } catch (error) {
    ctx.throw(500, error);
  }
}

async function create (ctx) {
  const newDevive = new Device(ctx.request.body)
  const savedDevice = await newDevive.save()
  ctx.body = savedDevice
}

async function destroy (ctx) {
  const id = ctx.params.id
  const device = await Device.findById(id)

  const deletedDevice = await device.remove()
  ctx.body = deletedDevice
}

async function update (ctx) {
  const mac = ctx.params.id
  const {row_index, agency_code, user_key, update_para, level} = ctx.request.body;
  let user_level = "2";
  let user_id="";
  try {
    // check agency code
    user_query = { $or: [{agency_code:agency_code}, {seller_code:agency_code}] };
    //user_query = {agency_code:agency_code};
    if(user_key!=null && user_key!=="")
      user_query["_id"] = mongoose.Types.ObjectId(user_key);
    const userData = await User.findOne(user_query)
    //console.log("[%s]", userData)
    if(!userData)
    {
      logger.info("%s [%s] Update Device. Invalid request! User Key [%s] Agency Code [%s]",
         ctx.method, ctx.ip, user_key, agency_code)
      ctx.status = 401;
      ctx.body = {
        message: "Not Auth"
      }
      return;
    }
    user_level = userData.level;
    user_id = userData.id;
  } catch (error) {
     ctx.throw(500, error);
     return;
  }

  // 뭔가 문제가 있는 사용자
  if(user_level!=level)
  {
    logger.info("%s [%s] Update Device. Invalid request! User ID [%s] Agency Code [%s] User Level [%s:%s]", 
        ctx.method, ctx.ip, user_id, agency_code,  user_level, level)
    ctx.status = 401;
    ctx.body = {
      message: "Not Auth"
    }
    return;
  }

  let query = {}
  if(user_level=="0")
    query = {mac:mac}
  else if(user_level=="1")
    query = {mac:mac, agency_code:agency_code}
  else if(user_level=="2")
    query = {mac:mac, seller_code:agency_code}
    
  const device = await Device.findOne(query)

  if(!device)
  {
    logger.info("%s [%s] Update Device. User ID:%s, Agency Code:%s, User Level:%s, MAC:%s, No data!", 
        ctx.method, ctx.ip, user_id, agency_code, user_level, mac);
    ctx.status = 404;
    ctx.body = {
      message: "입력값 확인 후 다시 시도해주세요."
    }
    return;
  }

  console.log("MAC:",mac, agency_code, update_para);
  const { username, usertel, userorderdate, vpn_off, remark, address, new_seller_code} = update_para;

  //console.log(username, usertel, userorderdate, vpn_off, remark, address);

  // ######## 수정 필요
  {
    username && (device.agency_data.username = username);
    usertel && (device.agency_data.usertel = usertel);
    userorderdate && (device.agency_data.order_date = userorderdate);
    remark && (device.agency_data.remark = remark);
    address && (device.agency_data.address = address);
    vpn_off && (device.vpn_off = vpn_off);
    new_seller_code && (user_level=="1") && (device.seller_code = (new_seller_code=="NONE") ? "" : new_seller_code);
    //device.done = !device.done
  }

  // Update todo in database
  const updatedDevice = await device.save();
  if(updatedDevice)
  {
    let device_mac = updatedDevice.mac;
    let traffic_query = {mac : device_mac}
    let max_traffic = 15;

    const trafficInfo = await DeviceTraffic.find(
      traffic_query,
      {traffic_date:1, mac:1, download:1, upload:1, ssr_speed:1, last_activated_status:1, ip_update_count:1, activation_count:1} 
    ).sort({traffic_date:-1}).limit(max_traffic);

    const updated_data = makeReturnDevice(row_index, updatedDevice, trafficInfo, user_level);
    ctx.status = 200;
    ctx.body = {
      message: "OK",
      row_index : row_index || 0,
      updated_data : updated_data
    }
    logger.info("%s [%s] Update Device. User ID:%s, Agency Code:%s, User Level:%s, MAC:%s, VPN OFF:%s, Succeed!", 
        ctx.method, ctx.ip, user_id, agency_code, user_level, mac, vpn_off);
  }
  else
  {
    ctx.status = 500;
    ctx.body = {
      message: "처리중 오류발생!"
    }    
    logger.error("%s [%s] Update Device. User ID:%s, Agency Code:%s, User Level:%s, MAC:%s, System Error!", 
        ctx.method, ctx.ip, user_id, agency_code, user_level, mac);
  }
}
 
 module.exports = {
   findAll,
   create,
   destroy,
   update
 }