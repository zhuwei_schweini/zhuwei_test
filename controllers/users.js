const User = require('../models/users')
const Device = require('../models/device')
const crypto = require('crypto');
const password_hashkey = "HsadkfasdlkfjasldXfjalsdjf1lksdfas";
const Joi = require('joi');
const jwt = require("../lib/jwt");
const moment = require('moment')
var logger = global.logger;
var config = global.config;

async function findAll (ctx) {
  const users = await User.find({}, {password:0})
  ctx.body = users
}

async function find (ctx) {
  const users = await User.find({}, {password:0})
  ctx.body = users
}

async function login (ctx) {
  const schema = Joi.object().keys({
     id : Joi.string().required(),
     password : Joi.string().required()
  });

  const result = Joi.validate(ctx.request.body, schema);
  if(result.error) 
  {
    logger.info("%s [%s] Invalid JWT Session.", ctx.method, ctx.ip)
    ctx.status = 400;
    ctx.body = result.error;
    return
  }

  const {id, password} = ctx.request.body;
  if(id.trim().length==0 || password.trim().length==0)
  {
    ctx.status = 401;
    ctx.body = {
      message: "입력값 확인 후 다시 시도해주세요."
    }
    return;
  }

  var hmac = crypto.createHmac("sha512", password_hashkey);
  hmac.update(id+password);
  password_auth = hmac.digest("hex");

  if(config.isProduction(config)==false)
    logger.debug("id:%s, password:%s, auth:%s", id, password, password_auth);

  try {
    const userData = await User.findOne({id:id, password:password_auth}, {password:0})
    if(!userData)
    {
      logger.info("%s [%s] Login Fail:%s.", ctx.method, ctx.ip, id)
      ctx.status = 401;
      ctx.body = {
        message: "입력값 확인 후 다시 시도해주세요."
      }
      return;
    }

    const online_time = process.env.ONLINE_SESSION_TIME || "2";
    var activatedTime = moment().add(parseInt(online_time)*-1, 'hours');
    const regexval_error = new RegExp("v0")
    let device_query = {};

    if(userData.level=="1")
      device_query["agency_code"] = userData.agency_code;
    else if(userData.level=="2")  // level 2의 경우 agency_code가 seller_code이다. 
      device_query["seller_code"] = userData.seller_code;

    const total_device = await Device.countDocuments(device_query);

    device_query['last_activated_date'] = {$gte: activatedTime};
    const active_device = await Device.countDocuments(device_query);

    device_query['last_activated_status'] = {$regex: regexval_error};
    const active_error_device = await Device.countDocuments(device_query);
    userData.last_login_date = Date.now();
    if(userData.login_count)
    userData.login_count++;
    else 
    userData.login_count = 1;

    let payload = {
      id:id, 
      level:userData.level || "2",
      agency_code : userData.agency_code || "NONE",
      seller_code : userData.seller_code || "NONE"
    }
    let jwttoken = jwt.issue(payload)
    const updateUser = await userData.save();

    ctx.set("Access-Control-Expose-Headers", "Total-Device, Active-Device, Active-Error-Device");
    ctx.set("Total-Device", total_device.toString() || "0");
    ctx.set("Active-Device", active_device.toString() || "0");
    ctx.set("Active-Error-Device", active_error_device.toString() || "0");

    let seller_list = [];
    let agency_list = [];

    if(userData.level=="0")
    {
      const agencyData = await User.find({level:"1"}, {_id:0, name:1, agency_code:1});
      if(agencyData)
      {
        agency_list = agencyData;
      }
    }

    if(userData.level=="1")
    {
      const sellerData = await User.find({agency_code:userData.agency_code,level:"2"}, {_id:0, name:1, seller_code:1});
      if(sellerData)
      {
        seller_list = sellerData;
      }
    }
    
    // code 재사용을 위해서 seller의 경우 리턴값을 수정한다. 
    if (userData.level=="2")
    {
      userData.parent_agency_code = userData.agency_code;
      userData.agency_code = userData.seller_code;
    }

    ctx.body = {
      token: jwttoken,
      user : userData,
      seller_list : seller_list,
      agency_list : agency_list
    }
   logger.info("%s [%s] Login Succeed! ID:%s. Agency_Code:%s, Total Device:%d, Active Device:%d, Error Device:%d, agency_list:%s, seller_list:%s", 
    ctx.method, ctx.ip, id, userData.agency_code, 
    total_device, active_device,active_error_device,
    agency_list.toString(), seller_list.toString()
    );
  } catch (error) {
    ctx.throw(error, 500);    
  }
}

async function create (ctx) {
  const newUser = new User(ctx.request.body)
  const savedUser = await newUser.save()
  ctx.body = savedUser
}

async function destroy (ctx) {
  const id = ctx.params.id
  const user = await User.findById(id)

  const deletedUser = await user.remove()
  ctx.body = deletedUser
}

async function update (ctx) {
  const id = ctx.params.id
  const user = await User.findById(id)

  // ######## 수정 필요
  {
    //device.done = !device.done
  }

  // Update todo in database
  const updatedUser = await user.save()
  ctx.body = updatedUser
}

module.exports = {
  findAll,
  find,
  create,
  destroy,
  update,
  login
}
