const moment = require('moment')
const mongoose = require('mongoose');
const axios = require("axios");
const appKey = require('../models/appKey')
const cityID = require('../models/cityId')
const ipLoc = require('../models/ipLocation')
const weather = require('../models/weather')
const weatherDev = require('../models/weatherDev')
const logger = global.logger;
const config = global.config;
const weather_timeLimit = 60; // 60 minuts
var d2d = require('degrees-to-direction');

// true : expired
// false : not expired. it is valid.
function check_weather_expired(weather_expire)
{
    var today = moment();
    var expired = moment(weather_expire);
    var diff = expired.diff(today, 'minute');

    // time is passed
    if(diff<0)
        return true;
    return false;   
}

async function get_ip_location(ip)
{
  const url = "http://api.ipstack.com/"+ip+"?access_key="+config.IPLOC_KEY;
  try {
    const response = await axios.get(url);
    const data = response.data;
    //console.log(data);
    return data;
  } catch (error) {
    console.log(error);
  }
  return null;
}

async function get_openweather_by_city(city)
{
  const url = "http://api.openweathermap.org/data/2.5/weather?q="+city+"&APPID="+config.OPENWEATHER_KEY+"&units=metric";
  try {
    const response = await axios.get(url);
    const data = response.data;
    console.log(">>>>> weather by city name >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    console.log(data);
    return data;
  } catch (error) {
    console.log(error);
  }
  return null;
}

async function get_openweather_by_id(id)
{
  const url = "http://api.openweathermap.org/data/2.5/weather?id="+id+"&APPID="+config.OPENWEATHER_KEY+"&units=metric";
  try {
    const response = await axios.get(url);
    console.log(">>>>> weather by city id >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    const data = response.data;
    console.log(data);
    return data;
  } catch (error) {
    console.log(error);
  }
  return null;
}

async function get_openweather_by_coord(lon, lat)
{
  const url = "http://api.openweathermap.org/data/2.5/weather?lat="+lat+"&lon="+lon+"&APPID="+config.OPENWEATHER_KEY+"&units=metric"
  
  
  ;
  try {
    const response = await axios.get(url);
    const data = response.data;
    console.log(">>>>> weather by city coord >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
    console.log(data);
    return data;
  } catch (error) {
    console.log(error);
  }
  return null;
}


/*
WEATHER _Weather[]={
    {0,"Fine",MCU_BMP_W_SUN},
    {1,"Fine",MCU_BMP_W_SUN},
    {2,"Fine",MCU_BMP_W_SUN},
    {3,"Fine",MCU_BMP_W_SUN},
    {4,"Cloudy",MCU_BMP_W_CLOUDY},
    {5,"Partly Cloudy",MCU_BMP_W_CLOUDY},
    {6,"Partly Cloudy",MCU_BMP_W_CLOUDY},
    {7,"Mostly Cloudy",MCU_BMP_W_CLOUDY},
    {8,"Mostly Cloudy",MCU_BMP_W_CLOUDY},
    {9,"Overcast",MCU_BMP_W_OVERCAST},
    {10,"Shower",MCU_BMP_W_RAIN},
    {11,"Thunder Shower",MCU_BMP_W_RAIN},
    {12,"Thunder Shower with hail",MCU_BMP_W_RAIN},
    {13,"Light Rain",MCU_BMP_W_RAIN},
    {14,"Middle Rain",MCU_BMP_W_RAIN},
    {15,"Heavy Rain",MCU_BMP_W_RAIN},
    {16,"Rainstorm",MCU_BMP_W_RAIN},
    {17,"Great Rainstorm", MCU_BMP_W_RAIN},
    {18,"Great Rainstorm", MCU_BMP_W_RAIN},
    {19,"Frozen Rain", MCU_BMP_W_RAIN},
    {20,"Sleet", MCU_BMP_W_SNOW},
     {21,"Snow", MCU_BMP_W_SNOW},
     {22,"Little Snow", MCU_BMP_W_SNOW},
     {23,"Mid Snow", MCU_BMP_W_SNOW},
     {24,"Big Snow", MCU_BMP_W_SNOW},
     {25,"Blizzard", MCU_BMP_W_SNOW},
     {26,"Floating dust", MCU_BMP_W_DUST},
    {27,"Dust", MCU_BMP_W_DUST},
     {28,"Sandstorm", MCU_BMP_W_DUST},
     {29,"Strong sandstorm", MCU_BMP_W_DUST},
     {30, "Fog", MCU_BMP_W_FOGGY},
     {31,"Smog", MCU_BMP_W_HAZE},
     {32, "Wind", MCU_BMP_W_WINDY},
     {33, "Gale", MCU_BMP_W_WINDY},
     {34, "Hurricane", MCU_BMP_W_WINDY},
     {35, "Tropical Storm", MCU_BMP_W_WINDY},
     {36, "Tornado", MCU_BMP_W_WINDY},
     {99, "Unknown", MCU_BMP_W_UNKNOWN},
};
*/

function get_code_by_openweather(code)
{
  switch(code)
  {
    case 800: 
      return "0"; //"Fine"
/*      
    case 200: 
      return "1"; //"Fine"
    case 200: 
      return "2"; //"Fine"
    case 200: 
      return "3"; //"Fine"
*/
    case 801: 
      return "4"; //"Cloudy"

    case 802: 
      return "5"; //"Partly Cloudy"
/*
    case 803: 
      return "6"; //"Partly Cloudy"
*/
    case 803: 
      return "7"; //"Mostly Cloudy"
/*      
    case 804: 
      return "8"; //"Mostly Cloudy"
*/
    case 804: 
      return "9"; //"Overcast"

    /////////////////////////////////////////////
    case 300: // dizzle
    case 301: 
    case 302: 
    case 310: 
    case 311: 
    case 312: 

    /////////////////////////////////////////////
    case 313: 
    case 314: 
    case 321: 
    case 521: 
    case 520:
      return "10"; //"Shower"

    /////////////////////////////////////////////
    case 200: 
    case 211: 
    case 522: 
      return "11"; //"Thunder Shower"

    /////////////////////////////////////////////
    case 531:
      return "12"; //"Thunder Shower with hail"

    /////////////////////////////////////////////
    case 500:
      return "13"; //"Light Rain"

    /////////////////////////////////////////////
    case 501:
      return "14"; //"Middle Rain"

    /////////////////////////////////////////////
    case 502:
    case 503:
      return "15"; //"Heavy Rain"

    /////////////////////////////////////////////
    case 504:
      return "16"; //"Rainstorm"

    /////////////////////////////////////////////
    /*
    case 200: 
      return "17"; //"Great Rainstorm"

    case 200: 
      return "18"; //"Great Rainstorm"
      */
    /////////////////////////////////////////////
    case 511: 
      return "19"; //"Frozen Rain"

    /////////////////////////////////////////////

    /////////////////////////////////////////////
    case 611: 
    case 612: 
    case 613: 
      return "20"; //"Sleet"

    /////////////////////////////////////////////
    case 615: 
    case 616: 
    case 620: 
    case 621: 
    case 622: 
      return "21"; //"Snow"

    /////////////////////////////////////////////
    case 600: 
     return "22"; //"Little Snow"

    /////////////////////////////////////////////
    case 601: 
      return "23"; //"Mid Snow"

    /////////////////////////////////////////////
    case 602: 
      return "24"; //"Big Snow"

    /////////////////////////////////////////////
    //case 762:  volcanic ash
 
    /////////////////////////////////////////////
    //case 200: 
    //  return "25"; //"Blizzard"
 
    /////////////////////////////////////////////
    case 731: 
      return "26"; //"Floating dust"
 
    /////////////////////////////////////////////
    case 761: 
      return "27"; //"Dust"

    /////////////////////////////////////////////
    case 751: 
      return "28"; //"Sandstorm"

    /////////////////////////////////////////////
    //case 200: 
    //  return "29"; //"Strong sandstorm"

    /////////////////////////////////////////////
    case 701: 
    case 721: 
    case 741: 
      return "30"; //"Fog"

    /////////////////////////////////////////////
    case 711: 
      return "31"; //"Smog"

    /////////////////////////////////////////////
    //case 200: 
    //  return "32"; //"Wind"

    /////////////////////////////////////////////
    case 771: 
      return "33"; //"Gale"

    /////////////////////////////////////////////
    //case 200: 
    //  return "34"; //"Hurricane"

    /////////////////////////////////////////////
    //case 200: 
    // return "35"; //"Tropical Storm"

    /////////////////////////////////////////////
    case 781: 
      return "36"; //"Tornado"
  }

  return "99"; //Unknown
}


function get_weather_text(code)
{
  switch(code)
  {
    case "0" : return "Fine";
    case "1" : return "Fine";
    case "2" : return "Fine";
    case "3" : return "Fine";

    case "4" : return "Cloudy";
    case "5" : return "Partly Cloudy";
    case "6" : return "Partly Cloudy";
    case "7" : return "Mostly Cloudy";
    case "8" : return "Mostly Cloudy";
    case "9" : return "Overcast";

    case "10" : return "Shower";
    case "11" : return "Thunder Shower";
    case "12" : return "Thunder Shower with hail";

    case "13" : return "Light Rain";
    case "14" : return "Middle Rain";

    case "15" : return "Heavy Rain";
    case "16" : return "Rainstorm";
    case "17" : return "Great Rainstorm";
    case "18" : return "Great Rainstorm";

    case "19" : return "Frozen Rain";
    case "20" : return "Sleet";
    case "21" : return "Snow";
    case "22" : return "Little Snow";

    case "23" : return "Mid Snow";
    case "24" : return "Big Snow";

    case "25" : return "Blizzard";

    case "26" : return "Floating dust";
    case "27" : return "Dust";
    case "28" : return "Sandstorm";
    case "29" : return "Strong Sandstorm";

    case "30" : return "Fog";
    case "31" : return "Smog";
    case "32" : return "Wind";
    case "33" : return "Gale";
    case "34" : return "Hurricane";
    case "35" : return "Tropical Storm";
    case "36" : return "Tornado";
  }

  return "99"; //Unknown
}

function get_wind_dir_by_openweather(deg)
{
  return d2d(deg)
}

async function update_weather_info(ctx, device_id, weatherData, newWeatherData)
{
  logger.info("%s [%s] Weather Check: got new weather then update. city id [%s], city name [%s], Device [%s]",
    ctx.method, ctx.ip, newWeatherData.id, newWeatherData.name, device_id);
  // update
  if(weatherData!=null)
  {
    weatherData.weather = newWeatherData.weather;
    weatherData.base = newWeatherData.base;
    weatherData.main = newWeatherData.main;
    weatherData.visibility = newWeatherData.visibility;
    weatherData.wind = newWeatherData.wind;
    weatherData.clouds = newWeatherData.clouds;
    weatherData.sys = newWeatherData.sys;
    expired_time = moment();
    expired_time.add(weather_timeLimit, "minute");
    weatherData.expired_time = expired_time;
    weatherData.last_update_date = moment();
    weatherData.update_count = weatherData.update_count + 1; 
    var updatedWeatherData = await weatherData.save();
    if(updatedWeatherData!=null)
      weatherData = updatedWeatherData;
  }
  else
  {
    // add new weather info
    newWeatherData.geo_location = {
      type: "Point",
      coordinates: [newWeatherData.coord.lon, newWeatherData.coord.lat]
    }
    expired_time = moment();
    expired_time.add(weather_timeLimit, "minute");
    newWeatherData.expired_time = expired_time;
    //newWeatherData.sys.type_id = newWeatherData.sys.type;

    const newWeather = new weather(newWeatherData);
    weatherData = await newWeather.save();
    logger.info("%s [%s] Weather Check: registered new weather! Device [%s], City Id[%s], City Name[%s]",
      ctx.method, ctx.ip, device_id, newWeatherData.id, newWeatherData.name)
  }

  return weatherData;
}

async function get_weather_by_name(ctx, device_id, city_name)
{
  var query = { name: { $regex : new RegExp(city_name, "i") }  };
  var need_weather_update = false;
  if(city_name==null || city_name=="") return null;

  var weatherData = await weather.findOne(query);
  var newWeatherData = null;
  if(weatherData)
  {
    if(check_weather_expired(weatherData.expired_time)) 
    {
      logger.debug("%s [%s] Weather Check: get_weather_by_name [%s]. weather is expired. it will be updated. Device [%s], City Name[%s]",
        ctx.method, ctx.ip, city_name,
        device_id, weatherData.name);
      need_weather_update=true;
    }
  }
  else
    need_weather_update=true;

    if(need_weather_update==true)
  {
    newWeatherData = await get_openweather_by_city(city_name);
    if(newWeatherData!=null)
    {
      logger.info("%s [%s] Weather Check: got new weather by city. Id[%s] City[%s], Tempp[%s], Hum[%s]",
        ctx.method, ctx.ip, newWeatherData.id, newWeatherData.name, newWeatherData.main.temp.toString(),  newWeatherData.main.humidity.toString());
      weatherData = update_weather_info(ctx, device_id, weatherData, newWeatherData);
    }
  }

  return weatherData;
}

async function get_weather_by_coord(ctx, device_id, city_lon, city_lat)
{
  var query = {  coord:{lon:city_lon,lat:city_lat} };
  var need_weather_update = false;

  if(city_lat==null || city_lon==null) return null;

  var weatherData = await weather.findOne(query);
  var newWeatherData = null;
  if(weatherData)
  {
    if(check_weather_expired(weatherData.expired_time)) 
    {
      logger.debug("%s [%s] Weather Check: get_weather_by_coord [%s, %s]. weather is expired. it will be updated. Device [%s], City Name[%s]",
        ctx.method, ctx.ip, city_lON.toString(), city_lat.toString(),
        device_id, weatherData.name);
      need_weather_update=true;
    }
  }
  else
    need_weather_update=true;

  if(need_weather_update==true)
  {
    newWeatherData = await get_openweather_by_coord(city_lon, city_lat);
    if(newWeatherData!=null)
    {
      logger.info("%s [%s] Weather Check: got new weather by coord. Id[%s] City[%s], Tempp[%s], Hum[%s]",
        ctx.method, ctx.ip, newWeatherData.id, newWeatherData.name, newWeatherData.main.temp.toString(),  newWeatherData.main.humidity.toString());
      weatherData = update_weather_info(ctx, device_id, weatherData, newWeatherData);
    }
  }

  return weatherData;
}

async function get_weather_by_id(ctx, device_id, city_id)
{
  var query = { id:city_id };
  var need_weather_update = false;
  if(city_id==null || city_id=="") return null;

  var weatherData = await weather.findOne(query);
  var newWeatherData = null;
  if(weatherData)
  {
    if(check_weather_expired(weatherData.expired_time)) 
    {
      logger.debug("%s [%s] Weather Check: get_weather_by_id [%s]. weather is expired. it will be updated. Device [%s], City Name[%s]",
        ctx.method, ctx.ip, city_id,
        device_id, weatherData.name);
      need_weather_update=true;
    }
  }
  else
    need_weather_update=true;

  if(need_weather_update==true)
  {
    newWeatherData = await get_openweather_by_id(city_id);
    if(newWeatherData!=null)
    {
      logger.info("%s [%s] Weather Check: got new weather by Id. Id[%s] City[%s], Tempp[%s], Hum[%s]",
        ctx.method, ctx.ip, newWeatherData.id, newWeatherData.name, newWeatherData.main.temp.toString(),  newWeatherData.main.humidity.toString());
      weatherData = update_weather_info(ctx, device_id, weatherData, newWeatherData);
    }
  }

  return weatherData;
}

async function get_weather_info(ctx, device_id, city_id, city_name, city_lon, city_lat)
{
  var weatherData = null;
  weatherData = await get_weather_by_id(ctx, device_id,  city_id);
  if(weatherData!=null) return weatherData;

  weatherData = await get_weather_by_name(ctx, device_id, city_name);
  if(weatherData!=null) return weatherData;

  weatherData = await get_weather_by_coord(ctx, device_id, city_lon, city_lat);

  return weatherData;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////
// get weatherDev
async function get_device_weather(ctx, is_autoreg_key, device_id, city_id, city_name, city_lat, city_lon)
{
  var clientIP = ctx.ip;
  var devData = null;
  var weatherData = null;
  var new_ip_loc = false;

  // check device id(device_id)
  try {
    // check agency code
    var query = { id:device_id };
    devData = await weatherDev.findOne(query);
    if(!devData)
    {
      if(is_autoreg_key==true)
      {
        // register new weather device
        var newWeatherDev = { id:device_id, public_ip: clientIP};
        const newDevive = new weatherDev(newWeatherDev);
        devData = await newDevive.save();
        logger.info("%s [%s] Weather Check: registered new weather device! Device [%s]",
          ctx.method, ctx.ip, device_id)
        new_ip_loc = true;
      }
      else
      {
        logger.info("%s [%s] Weather Check: Invalid request(DEVID)! No Device [%s]",
          ctx.method, ctx.ip, device_id)
        ctx.status = 401;
        ctx.body = {
          message: "Not Auth"
        }
        return null;        
      }
    }
  } catch (error) {
     ctx.throw(500, error);
     return null;
  }

  // check public ip
  try {
    if(new_ip_loc==true || devData.public_ip!=clientIP) // new public ip is detected. 
    {
      devData.public_ip = clientIP;
      // find ip info from local db
      var query = { ip:clientIP };
      var ipData = await ipLoc.findOne(query);
      if(!ipData)
      {
        // update public ip and information
        const newIPLocData = await get_ip_location(clientIP);
        if(newIPLocData!=null)
        {
          const newIpLoc = new ipLoc(newIPLocData);
          ipData = await newIpLoc.save();
        }
      }
      if(ipData)
      {
        devData.ip_country_code = ipData.country_code;
        devData.ip_city_name = ipData.city;
        devData.ip_geo_lat = ipData.latitude;
        devData.ip_geo_lon = ipData.longitude;
        devData.update_count = devData.update_count + 1; 

        logger.info("%s [%s] Weather Check: Updated ip location! Device [%s], City[%s]",
          ctx.method, ctx.ip, device_id, devData.ip_city_name)
      }
      else
      {
        logger.info("%s [%s] Weather Check: failed to get ip location! Device [%s]",
          ctx.method, ctx.ip, device_id)
      }
    }
  } catch (error) {
    ctx.throw(500, error);
    return null;
  }

  logger.debug("%s [%s] Weather Check: get weather Device [%s], Id[%s] Name[%s] lon[%s], lat[%s]",
   ctx.method, ctx.ip, device_id, city_id, city_name, city_lon, city_lat);

  // no input key values
  if((city_id==null || city_id=="") && (city_name==null || city_name=="") && (city_lat=="" || city_lon==""))
  {
    // use device info
    if(devData!=null)
    {
      city_id = devData.weather_city_id;
      city_name = devData.weather_city_name;
      if(city_name==null || city_name=="")
        city_name = devData.ip_city_name;

      city_lat = devData.weather_city_lat;
      city_lon = devData.weather_city_lon;
      
      if(city_lat==null || city_lon==null)
      {
        city_lat = devData.ip_geo_lat;
        city_lon = devData.ip_geo_lon;
      }
    }
  }
  logger.debug("%s [%s] Weather Check: get weather Device2 [%s], Id[%s] Name[%s] lon[%s], lat[%s]",
   ctx.method, ctx.ip, device_id, city_id, city_name, city_lon, city_lat);

  weatherData = await get_weather_info(ctx, device_id, city_id, city_name, city_lon, city_lat);
  if(weatherData!=null)
  {
    logger.info("%s [%s] Weather Check: got weather! Device [%s], Id[%s], City[%s]",
      ctx.method, ctx.ip, device_id,
      weatherData.id, weatherData.name);
     
    // update
    if(devData.ip_city_name==null) devData.ip_city_name = weatherData.name;
    devData.weather_city_id = weatherData.id;
    devData.weather_city_name = weatherData.name;
    devData.weather_geo_lat = weatherData.coord.lat;
    devData.weather_geo_lon = weatherData.coord.lon;
    devData.weather_country_code = weatherData.sys.country;

    var weather_code =get_code_by_openweather(weatherData.weather[0].id);
    var weather_text =get_weather_text(weather_code);

    devData.weather_now.code = weather_code;
    devData.weather_now.wind_scale = Math.round(weatherData.wind.speed).toString();
    devData.weather_now.temperature = Math.round(weatherData.main.temp).toString();
    devData.weather_now.humidity = Math.round(weatherData.main.humidity).toString();
    devData.weather_now.wind_direction = get_wind_dir_by_openweather(weatherData.wind.deg);
    devData.weather_now.text = weather_text;
    devData.last_update_date = weatherData.last_update_date;
  }
  else
  {
    logger.info("%s [%s] Weather Check: failed to get weather! Device [%s], Id[%s], City[%s]",
      ctx.method, ctx.ip, device_id, city_id, city_name);
    ctx.status = 404;
    ctx.body = {
      message: "Not Found"
    }
      return null;      
  }

  return devData;
}

async function check (ctx) {
  const device_id = ctx.params.id
  var {app_key, city_id, city_name, city_lat, city_lon} = ctx.request.body;
  var   is_autoreg_key = true;
  var   devData = null;

  //if(clientIP=="::1") clientIP = "8.8.8.8";

  if(city_lat==null) city_lat="";
  if(city_lon==null) city_lon="";
  
  // check default validataion: device_id, app_key
  if(device_id==null || device_id=="" || app_key==null || app_key=="")
  {
    logger.info("%s [%s] Weather Check: Invalid request! Device [%s] key [%s]",
            ctx.method, ctx.ip, device_id, app_key)
    ctx.status = 401;
    ctx.body = {
      message: "Not Auth"
    }
     return;
  }

  devData = await get_device_weather(ctx, is_autoreg_key, device_id, city_id, city_name, city_lat, city_lon);
  if(devData!=null)
  {
    // Update weather
    const updatedDevData = await devData.save();
    if(updatedDevData)
    {
      var retData = {  
        last_update: moment(updatedDevData.last_update_date).format('YYYY-MM-DDTHH:mm:ssZZ'),  // 2019-03-01T16:30:00+08:00
        now : {
          code: updatedDevData.weather_now.code ,
          wind_scale: updatedDevData.weather_now.wind_scale,
          temperature: updatedDevData.weather_now.temperature,
          humidity: updatedDevData.weather_now.humidity,
          wind_direction: updatedDevData.weather_now.wind_direction,
          text: updatedDevData.weather_now.text
        },
        name: updatedDevData.weather_city_name,
        cityid: updatedDevData.weather_city_id,
        county: updatedDevData.weather_country_code
      };      
      ctx.status = 200;
      ctx.body = {
        error: "0",
        data : retData
      };
      logger.info("%s [%s] Weather Check: Device [%s], Country[%s], City[%s], CityId[%s], Code[%s], Temp[%s], Hum[%s], Wind Scale[%s] Dir[%s]",
        ctx.method, ctx.ip, device_id, retData.county, retData.name, retData.cityid,
        retData.now.code,
        retData.now.temperature,
        retData.now.humidity,
        retData.now.wind_scale,
        retData.now.wind_direction
      );
    }
    else
    {
      ctx.status = 500;
      ctx.body = {
        message: "System Error!"
      }    
      logger.info("%s [%s] Weather Check: failed to get weather! Device [%s]",
        ctx.method, ctx.ip, device_id)
    }
  }
}

async function check_get (ctx) {
  var app_key = ctx.get('Authorization');
  var city_id = ctx.request.query.city;
  var device_id = ctx.request.query.mac;
  var city_name = "";
  var city_lat = "";
  var city_lon = "";
  var is_autoreg_key = true;
  var devData = null;
  var weatherData = null;

  if(city_id==null) city_id = "";
  if(device_id==null) device_id = "";

  // check default validataion: device_id, app_key
  if(app_key==null || app_key=="" )
  {
    logger.info("%s [%s] Weather Check: Invalid request! Device [%s] city [%s]",
            ctx.method, ctx.ip, device_id, city_id)
    ctx.status = 401;
    ctx.body = {
      message: "Not Auth"
    }
     return;
  }

  // check api key
  try {
    // check agency code
    var query = { key:app_key };
    const keyData = await appKey.findOne(query);
    if(!keyData)
    {
      logger.info("%s [%s] Weather Check: Invalid request(APPKEY)! Device [%s] city [%s] key [%s]",
        ctx.method, ctx.ip, device_id, city_id, app_key)
      ctx.status = 401;
      ctx.body = {
        message: "Not Auth"
      }
      return;
    }

    // if is_multi is false, device_id should be matched also
    if(keyData.is_multiple==false && keyData.device_id!=device_id)
    {
      logger.info("%s [%s] Weather Check: Invalid request(DEVID)! Device [%s] city [%s] key [%s]",
        ctx.method, ctx.ip, device_id, city_id, api_key)
      ctx.status = 401;
      ctx.body = {
        message: "Not Auth"
      }
      return;
    }
    is_multiple_key = keyData.is_multiple;
    is_autoreg_key = keyData.is_autoreg;
  } catch (error) {
     ctx.throw(500, error);
     return;
  }

  // device id를 기준으로 처리 
  if(device_id!="")
  {
    devData = await get_device_weather(ctx, is_autoreg_key, device_id, city_id, city_name, city_lat, city_lon);
    if(devData!=null)
    {
      // Update weather
      const updatedDevData = await devData.save();
      if(updatedDevData)
      {
        var retData = {  
          last_update: moment(updatedDevData.last_update_date).format('YYYY-MM-DDTHH:mm:ssZZ'),  // 2019-03-01T16:30:00+08:00
          now : {
            code: updatedDevData.weather_now.code ,
            wind_scale: updatedDevData.weather_now.wind_scale,
            temperature: updatedDevData.weather_now.temperature,
            humidity: updatedDevData.weather_now.humidity,
            wind_direction: updatedDevData.weather_now.wind_direction,
            text: updatedDevData.weather_now.text
          },
          name: updatedDevData.weather_city_name,
          cityid: updatedDevData.weather_city_id,
          county: updatedDevData.weather_country_code
        };
        
        ctx.status = 200;
        ctx.body = {
          error: "0",
          data : retData
        };
        logger.info("%s [%s] Weather Check: Device [%s], Country[%s], City[%s], CityId[%s], Code[%s], Temp[%s], Hum[%s], Wind Scale[%s] Dir[%s]",
          ctx.method, ctx.ip, device_id, retData.county, retData.name, retData.cityid,
          retData.now.code,
          retData.now.temperature,
          retData.now.humidity,
          retData.now.wind_scale,
          retData.now.wind_direction
        );
      }
      else
      {
        ctx.status = 500;
        ctx.body = {
          message: "System Error!"
        }    
        logger.info("%s [%s] Weather Check: failed to get weather! Device [%s]",
          ctx.method, ctx.ip, device_id)
      }
    }
  }
  else
  {
    weatherData = await get_weather_info(ctx, device_id, city_id, city_name, city_lon, city_lat);
    if(weatherData!=null)
    {
      logger.info("%s [%s] Weather Check: got weather! Device [%s], Id[%s], City[%s]",
        ctx.method, ctx.ip, device_id,
        weatherData.id, weatherData.name);

      var weather_code = get_code_by_openweather(weatherData.weather[0].id);
      var weather_text = get_weather_text(weather_code);

      var retData = {  
        last_update: moment(weatherData.last_update_date).format('YYYY-MM-DDTHH:mm:ssZZ'),  // 2019-03-01T16:30:00+08:00
        now : {
          code: weather_code ,
          wind_scale: Math.round(weatherData.wind.speed).toString(),
          temperature: Math.round(weatherData.main.temp).toString(),
          humidity: Math.round(weatherData.main.humidity).toString(),
          wind_direction: get_wind_dir_by_openweather(weatherData.wind.deg),
          text: weather_text
        },
        name: weatherData.name,
        cityid: weatherData.id,
        county:  weatherData.sys.country
      };
    
      ctx.status = 200;
      ctx.body = {
        error: "0",
        data : retData
      };
      logger.info("%s [%s] Weather Check: Device [%s], Country[%s], City[%s], CityId[%s], Code[%s], Temp[%s], Hum[%s], Wind Scale[%s] Dir[%s]",
        ctx.method, ctx.ip, device_id, retData.county, retData.name, retData.cityid,
        retData.now.code,
        retData.now.temperature,
        retData.now.humidity,
        retData.now.wind_scale,
        retData.now.wind_direction
    );
    }
    else
    {
      logger.info("%s [%s] Weather Check: failed to get weather! Device [%s], Id[%s], City[%s]",
        ctx.method, ctx.ip, device_id, city_id, city_name);
      ctx.status = 404;
      ctx.body = {
        message: "Not Found"
      }
        return;      
    }
  }
}

module.exports = {
  check,
  check_get
}