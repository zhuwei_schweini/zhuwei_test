module.exports = {
  apps : [{
    name: 'rest-wlinkapi',
    script: 'index.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    args: '',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development',
      DB: "mongodb://localhost/wlink",
      JWT_SECRET: "15567b09-88d2-45ba-9ddc-00314dde41a9"
    },
    env_production: {
      NODE_ENV: 'production',
      DB: "mongodb://172.26.9.73:27017,172.26.3.26:27017,172.26.40.122:27017/wlink",
      IPLOC_KEY: "c69c3adac5eee95f8f9078180bde0ff0",
      OPENWEATHER_KEY: "1aef6a78b0c9ffc201fadf9954babc25",
      WEATHERCOM_KEY: "",
      ONLINE_SESSION_TIME: 576,
      LOG_PATH: "logs"
    }
  }],

  deploy : {
    production : {
      user : 'ubuntu',
      host : ['api1.wavlink.xyz', 'api2.wavlink.xyz'],
      ref  : 'origin/master',
      repo : 'https://zhuwei_schweini@bitbucket.org/zhuwei_schweini/zhuwei_test.git',
      path : '/home/ubuntu/zhuwei/rest-wlinkapi',
      ssh_options: ['ForwardAgent=yes'],
      'post-deploy' : 'yarn && pm2 reload ecosystem.config.js --env production'
    },
    staging: {
      user : 'kobe',
      host : '192.168.16.188',
      ref  : 'origin/master',
      repo : 'https://zhuwei_schweini@bitbucket.org/zhuwei_schweini/zhuwei_test.git',
      path : '/home/kobe/zhuwei_test/rest-wlinkapi',
      ssh_options: ['ForwardAgent=yes'],
      'post-deploy' : 'yarn && pm2 reload ecosystem.config.js --env development'
    },
    dev: {}    
  }
};
