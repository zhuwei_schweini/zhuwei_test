require('dotenv').config();
const path = require('path');

// env to config
var config = 
{
  node_env : process.env.NODE_ENV || "development",
  port :  process.env.PORT || 4000,
  log_path : process.env.LOG_PATH || "/logs",
  db : process.env.DB,
  jwt_sceret : process.env.JWT_SECRET,
  session_time : parseInt(process.env.ONLINE_SESSION_TIME) || 2,
  path : path.join(__dirname),
  IPLOC_KEY : process.env.IPLOC_KEY,
  OPENWEATHER_KEY : process.env.OPENWEATHER_KEY,
  WEATHERCOM_KEY : process.env.WEATHERCOM_KEY,
  isProduction : (cfg) => cfg.node_env!=="development",
}

console.log ("CONFIG : NODE_ENV [%s]", process.env.NODE_ENV);
console.log ("CONFIG : DB [%s]", process.env.DB);
console.log ("CONFIG : PORT [%s]", config.port);
console.log ("CONFIG : JWT_SECRET [%s]", process.env.JWT_SECRET);
console.log ("CONFIG : IPLOC_KEY [%s]", config.IPLOC_KEY);
console.log ("CONFIG : OPENWEATHER_KEY [%s]", config.OPENWEATHER_KEY);
console.log ("CONFIG : WEATHERCOM_KEY [%s]", config.WEATHERCOM_KEY);
console.log ("CONFIG : ONLINE_SESSION_TIME [%s]", process.env.ONLINE_SESSION_TIME || "2");
console.log ("CONFIG : LOG PATH [%s]", process.env.LOG_PATH || "logs");

global.config = config;

// logger는 여기서 초기화
var Logger = require('./lib/logger');
var logger = new Logger('wlink_api');

logger.level = 'debug';
global.logger = logger;

const server = require('./server');
server.listen(config.port, () => console.log(`API server started on ${config.port}`))
