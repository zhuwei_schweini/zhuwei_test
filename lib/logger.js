var winston = require('winston');
const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
var DailyRotateFile = require('winston-daily-rotate-file');
var fs = require('fs');
var moment = require('moment');

const logDir = global.config.path + '/logs';

if(!fs.existsSync(logDir)) {
    fs.mkdirSync(logDir);
}

const logFormat = printf(info => {
  return `${info.timestamp} ${info.level}: ${info.message}`;
});

var logger = function(name) {
    var newLogger = createLogger({
      format: combine(
        format.splat(),
        format.simple(),
        timestamp({
          format: 'YYYY/MMDD HH:mm:ss'
        }
        ),
        logFormat
      ),
        transports: [
            new (transports.Console)({
                colorize: true
            }),
            new DailyRotateFile({
                filename: logDir+'/'+name+'_%DATE%.log',
                datePattern: 'YYYYMMDD',
                zippedArchive: true,
                maxSize: '20m',
                maxFiles: '14d'
            })
        ],
        exceptionHandlers: [
            new DailyRotateFile({
              filename: logDir+'/'+name+'_e_%DATE%.log',
              datePattern: 'YYYYMMDD',
              zippedArchive: true,
              maxSize: '20m',
              maxFiles: '14d'
          })
        ],
        exitOnError: false
    });

    return newLogger;
};

module.exports = logger;
module.exports.stream = {
    write: function(message, encoding) {
        logger.info(messgae);
    }
}
