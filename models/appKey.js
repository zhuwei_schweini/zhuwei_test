// models/appKey.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var appKeySchema = new Schema({
  key: String,
  device_id: String, 
  is_multiple: { type:Boolean, default: true },
  is_autoreg: { type:Boolean, default: true  },
  created_date : { type: Date, default: Date.now  },
}, { collection: 'appKey' });
 
appKeySchema.index({ key: 1 });
module.exports = mongoose.model('appKey', appKeySchema);