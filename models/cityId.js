// models/cityId.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var cityIdSchema = new Schema({
  name: String,
  lang: String, 
  id: String,
  geo_location : {
    type: { type: String },  // Point
    coordinates: []
  }, 
created_date : { type: Date, default: Date.now  },
}, { collection: 'cityId' });
 
cityIdSchema.index({ name: 1 });
cityIdSchema.index({ name: 1, lang: 1 });
cityIdSchema.index({ geo_location: "2dsphere" });

module.exports = mongoose.model('cityId', cityIdSchema);