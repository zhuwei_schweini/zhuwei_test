// models/ipLocation.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var ipLocSchema = new Schema({
  ip: String,
  type: String,
  continent_code: String,
  continent_name: String,
  country_code: String,
  country_name: String,
  region_code: String,
  region_name: String,
  city: String,
  zip : String,
  latitude: Number,
  longitude: Number,
  location : {
    geoname_id: Number,
    capital: String,
    languages:[{
      code: String,
      name: String,
      native: String,
    }],
    country_flag: String,
    country_flag_emoji: String,
    country_flag_emoji_unicode: String,
    calling_code: String,
    is_eu: Boolean
  },
  update_count: {type:Number, default:1 },
  last_update_date : { type: Date, default: Date.now  },
  created_date : { type: Date, default: Date.now  },
}, { collection: 'lpLocation' });
 
ipLocSchema.index({ ip: 1 });

module.exports = mongoose.model('lpLocation', ipLocSchema);