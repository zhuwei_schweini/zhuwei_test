// models/weather.js
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
 
var weatherSchema = new Schema({
  name: String, // english name
  id: String,   // unique id
  geo_location : {
    type: { type: String },  // Point
    coordinates: []
  }, 
  service: { type: String, default: "openweather" },
  coord:{lon:Number,lat:Number},
  weather:[
    {
      id: Number,
      main: String,
      description: String,
      icon: String
    }
  ],
  base: String,
  main: {
    temp: Number,
    pressure: Number,
    humidity: Number,
    temp_min: Number,
    temp_max :Number
  },
  visibility: Number,
  wind:{
    speed: Number,
    deg: Number
  },
  clouds:{
    all: Number
  },
  dt: Date,
  sys:{
    id: Number,
    message: Number,
    country: String,
    sunrise: Date,
    sunset: Date,
    type: { type: Number, default: 1  }
  },
  timezone: Number,
  cod: Number,
  expired_time: Date,
  update_count: {type:Number, default:1 },
  last_update_date : { type: Date, default: Date.now  },
  created_date : { type: Date, default: Date.now  },
}, { collection: 'weather' });
 
weatherSchema.index({ is: 1 });
weatherSchema.index({ name: 1 });
weatherSchema.index({ name: 1, lang: 1 });
weatherSchema.index({ geo_location: "2dsphere" });

module.exports = mongoose.model('weather', weatherSchema);