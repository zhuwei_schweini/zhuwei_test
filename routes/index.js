module.exports = (router) => {
  router.prefix('/v1')
  router.use('/weather/now', require('./weather'))
}
