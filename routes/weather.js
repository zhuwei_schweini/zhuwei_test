const Router = require('koa-router')
const router = new Router()
const Ctrl = require('../controllers/weather')

router.post('/:id', Ctrl.check)
router.get('/', Ctrl.check_get)

module.exports = router.routes()