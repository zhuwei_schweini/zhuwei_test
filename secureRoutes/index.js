const jwt = require("../lib/jwt");

module.exports = (router) => {
  router.prefix('/v1')
  router.use(jwt.errorHandler()).use(jwt.jwt());
  //router.use('/users', require('./users'))
  //router.use('/device', require('./device'))
}
