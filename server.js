const Koa = require('koa')
const Router = require('koa-router')
const Logger = require('koa-logger')
const Cors = require('@koa/cors')
const BodyParser = require('koa-bodyparser')
const Helmet = require('koa-helmet')
const respond = require('koa-respond')
const mongoose = require('mongoose');

const app = new Koa()
const router = new Router()
const securedRouter = new Router()

app.use(Helmet())

if (process.env.NODE_ENV === 'development') {
  app.use(Logger())
}

app.proxy = true;
app.use(Cors())
app.use(BodyParser({
  enableTypes: ['json'],
  jsonLimit: '5mb',
  strict: true,
  onerror: function (err, ctx) {
    ctx.throw('body parse error', 422)
  }
}))

app.use(respond())

// API routes
require('./routes')(router)
app.use(router.routes())
app.use(require('koa-static')('./build'))
app.use(router.allowedMethods())

require('./secureRoutes')(securedRouter)
app.use(securedRouter.routes())
app.use(securedRouter.allowedMethods())

//app.use(router.routes()).use(router.allowedMethods());
//app.use(securedRouter.routes()).use(securedRouter.allowedMethods());


mongoose.connect(process.env.DB, { useNewUrlParser: true })
mongoose.set('useCreateIndex', true)

module.exports = app
